# Starter Jekyll site

Starter Jekyll site for completely customised styles and content. Runs on a gulp workflow, with Sass and Javascript compilation. Browsersync included.

1. `git clone git@gitlab.com:huijing/jekyll-base.git`
2. `bundle update`
3. `yarn install`
4. `gulp`
