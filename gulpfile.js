var gulp        = require('gulp');
var browserSync = require('browser-sync');
var sass        = require('gulp-sass');
var prefix      = require('gulp-autoprefixer');
var cssnano     = require('gulp-cssnano');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var babel       = require('gulp-babel');
var cp          = require('child_process');

var messages = {
    jekyllDev: '<span style="color: grey">Running:</span> $ jekyll build for dev',
    jekyllProd: '<span style="color: grey">Running:</span> $ jekyll build for prod'
};

/**
 * Build the Jekyll Site in development mode
 */
gulp.task('jekyll-dev', function (done) {
  browserSync.notify(messages.jekyllDev);
  return cp.spawn('bundle', ['exec', 'jekyll', 'build', '--future', '--drafts', '--config', '_config.yml,_config_dev.yml'], {stdio: 'inherit'})
    .on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll-rebuild', ['jekyll-dev'], function () {
  browserSync.reload();
});

/**
 * Wait for jekyll-dev, then launch the Server
 */
gulp.task('browser-sync', ['sass', 'scripts', 'jekyll-dev'], function() {
  browserSync.init({
    server: "_site",
    port: 4987
  });
});

/**
 * Compile files from _sass into both _site/assets/css (for live injecting)
 * and assets/css (for future jekyll builds)
 */
gulp.task('sass', function () {
  return gulp.src(['_sass/index.scss'])
    .pipe(sass({
      includePaths: ['scss'],
      onError: browserSync.notify
    }))
    .pipe(prefix(['last 3 versions', '> 1%', 'ie 11'], { cascade: true }))
    .pipe(gulp.dest('_site/assets/css'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest('assets/css'));
});

/**
 * Compile files from _js into both _site/assets/js (for live injecting)
 * and assets/js (for future jekyll builds)
 */
gulp.task('scripts', function() {
  return gulp.src(['_js/lib/*.js', '_js/custom.js'])
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('_site/assets/js'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest('assets/js'));;
});

/**
 * Watch scss files for changes & recompile
 * Watch js files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 */
gulp.task('watch', function () {
  gulp.watch(['_sass/**/*.scss','_sass/*.scss'], ['sass']);
  gulp.watch(['_js/**/*.js','_js/*.js'], ['scripts']);
  gulp.watch(['index.html', '_layouts/*.html', '_drafts/*', '_posts/*', '_includes/*.html', '_pages/*'], ['jekyll-rebuild']);
});

/**
 * Build the Jekyll Site in production mode
 */
gulp.task('jekyll-prod', function (done) {
  browserSync.notify(messages.jekyllProd);
  return cp.spawn('bundle', ['exec', 'jekyll', 'build'], {stdio: 'inherit'})
    .on('close', done);
});

/**
 * Compile files from _sass into both _site/assets/css (for live injecting) 
 * and assets/css (for future jekyll builds)
 */
gulp.task('sass-prod', function () {
  return gulp.src(['_sass/index.scss'])
    .pipe(sass({
      includePaths: ['scss'],
      onError: browserSync.notify
    }))
    .pipe(prefix(['last 3 versions', '> 1%', 'ie 8'], { cascade: true }))
    .pipe(cssnano())
    .pipe(gulp.dest('_site/assets/css'))
    .pipe(gulp.dest('assets/css'));
});

/**
 * Compile files from _js into both _site/assets/js (for live injecting) and assets/js (for future jekyll builds)
 */
 gulp.task('scripts-prod', function() {
  return gulp.src(['_js/lib/*.js', '_js/custom.js'])
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest('_site/assets/js'))
    .pipe(gulp.dest('assets/js'));;
});

/**
 * Default task, running just `gulp` will compile the sass,
 * Compile the Jekyll site, launch BrowserSync & watch files.
 */
gulp.task('default', ['browser-sync', 'watch']);
gulp.task('build', ['sass-prod', 'scripts-prod', 'jekyll-prod']);
